
//  SettingsViewController.swift
//  Shrecordero
//  Created by FORTES Enterprise on 20.02.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit

class FakeLoadingController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        Thread.sleep(forTimeInterval: 2);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.performSegue(withIdentifier: "startSegue", sender: nil);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
