//
//  MarkerListTableViewCell.swift
//  Shrecordero
//
//  Created by FORTES Enterprise on 21.06.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit

class MarkerListTableViewCell: UITableViewCell {
    //Markers
    @IBOutlet weak var MarkerInfo1: UILabel!
    @IBOutlet weak var MarkerInfo2: UILabel!
    @IBOutlet weak var MarkerInfo3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
