//
//  MarkerListTableViewController.swift
//  Shrecordero
//
//  Created by FORTES Enterprise on 21.06.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit

class MarkerListTableViewController: UITableViewController {
    @IBOutlet weak var Stepper: UIStepper!
    var recording: Recording! = nil
    var RecViewCont: RecordingViewController! = nil
    var markers = [Flag]()
    var markerNr: Int = 0
    
    @IBAction func MarkerSelected() //Treba dokoncit !!!
    {
        self.Stepper.value = 0;
        let step = UIStepper()
        step.value = 0
        self.RecViewCont.StepperChange(step)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        FillMarkersInfo()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return markers.count
    }
    
    private func FillMarkersInfo(){
        for index in 0...self.recording.markedTimes.count-1
        {
            markers += [self.recording.markedTimes[index]]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MarkerListTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MarkerListTableViewCell else
        {exit(1)}
        let marker = markers[indexPath.row]
        
        cell.MarkerInfo1.text = marker.filename
        cell.MarkerInfo2.text = marker.group
        cell.MarkerInfo3.text = String(marker.start) + " : " + String(marker.end)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        markerNr = indexPath.row
        self.performSegue(withIdentifier: "RecordingViewControllerSegue", sender: markerNr)
    }
    
    func prepareForSegue(segue: UIStoryboardSegue, sender: Int?) {
        if segue.identifier == "RecordingViewControllerSegue" {
            if let nextVC = segue.destination as? RecordingViewController{
                nextVC.markerNr = sender!
            }
        }
    }
    
   // func tableView(didSelectRowAtIndexPath indexPath: IndexPath) {
   //     let SelectedMarker = items.ObjectAtIndex(indexPath.row) as String
    //}
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
