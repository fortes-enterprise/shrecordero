//
//  Recording.swift
//  Shrecordero
//
//  Created by FORTES Enterprise on 30.01.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import Foundation

class Recording
{
    var name: String = "NoName"
    var duration: Double = 0
    var filename: String = "NoFileNameIsProblem"
    var date: String = "NoTime"
    var markers: Int = 0
    var place: String = "NoWhere"
    var author: String = "NoOne"
    var shouldBeDeleted: String = "Yes"
    var markedTimes: [Flag] = []
    
    func printInfo()
    {
        print("***************")
        print("*Name: ",name)
        print("*FileName: ",filename)
        print("*Date: ",date)
        print("*Markers: ", markers)
        for e in markedTimes
        {
            print("*   ", e.defaultTime)
        }
        print("*Place: ",place)
        print("*Author: ",author)
        print("***************")
    }
}

class Flag
{
    var group: String = "No group"
    var defaultTime: Double = 0;
    var start: Double = 0;
    var end: Double = 0;
    var filename: String = "Unsaved";
}
