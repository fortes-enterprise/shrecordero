//
//  Recorder.swift
//  Shrecordero
//
//  Created by FORTES Enterprise on 18.01.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import Foundation
import AVFoundation

class Recorder: NSObject, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    //Debug
    var allowSaveAndLoadConsole: Bool = false;
    var allowListRecordings: Bool = false;
    var allowRecordingConsole: Bool = false;
    var allowPlaybackConsole: Bool = true;
    
    //RecorderStuff
    var recordedSound: AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var permission: Bool
    var availableRecordings: Int
    var recordings: [Recording] = []
    var flagAmount = 0
    var flags : [Flag] = []
    var recName: String = "";
    var partSound: AVPlayer? = nil;
    var dur: Double = 0;
    
    //Settings
    var defaultAuthor="Someone"
    var defaultPlace="Somewhere"
    var flagDuration: Int = 5
    
    enum State{
        case nothing
        case recording
        case playing
        case paused
    }
    
    var state: State
    
    override init()
    {
        state = State.nothing
        availableRecordings = 0
        permission=false;
        super.init();
        load();
        deleteUnsaved();
    }
    
    func deleteUnsaved()                                                //Delete unsaved recordings...
    {
        
        var useless: [Int] = []                                         //Makes an array of Ints
        
        var i: Int = 0;
        for recording in recordings {                                   //Loops through all recordings
            
            if recording.shouldBeDeleted == "Yes" {
                
            var toErase: [Int] = []                                     //Creates an aray of indexes (markers)
        
            deleteRecording(filename: recording.filename)               //Deletes recording according to its name
            recording.filename = "Deleted"                              //Changes the filename to deleted
        
            for index in 0...recording.markedTimes.count-1 {            //Loops through marked times(flags) in the current recording
                if recording.markedTimes[index].filename == "Unsaved"   //Checks if the index is saved
                {
                    toErase.append(index)                               //Push the index to array
                }
            }
        
            var mod: Int = 0
        
            for index in toErase                                        //Deletes unsaved markers
            {
                recording.markedTimes.remove(at: index-mod)
                recording.markers-=1;
                mod += 1
            }
                
                if recording.markers<1 {                                //Chceck if recording has any markers
                    useless.append(i);
                }
            
            }
            i += 1;
            
        }
        
        var mod: Int = 0
        
        for index in useless
        {
            recordings.remove(at: index-mod)                             //Delete recordings with no markers ??? Sure we are supposed to?
            availableRecordings-=1;
            mod += 1
        }
        
        save();
    }
    
    func addFlag()                                                      //Add flag to recording
    {
        if state == State.recording                                     //Possible only during recording
        {
            flagAmount += 1;
            let flag = Flag();                                          //Make object
            flag.defaultTime = audioRecorder.currentTime;               //Set flag default time as current
            flag.start = flag.defaultTime - Double(flagDuration);       //Flag start is 5 min. before current time
            if flag.start < 0 {                                         //In case that someone place a flag in the first 5 min.
                flag.start = 0;
            }
            flag.end = flag.defaultTime + Double(flagDuration);         //Flag will end 5 min. after current time
            flags.append(flag);                                         //Send flag to his family :D
        }
    }
    
    
    func startRecording(){
        state = State.recording
        
        var audioFilename = getDocumentDirectory().appendingPathComponent("klofik")     //Declaration of audioFilename var

            var name: String = "Shrecording";
            var i: Int = 0;
        var found: Bool = false;
            while(!found)
            {
            name.append(String(i))                                                      //Append name by i
            name.append(".m4a");                                                        //Appends audio file postfix
                
            recName = name;                                                             //Names the recording
                
            audioFilename = getDocumentDirectory().appendingPathComponent(name)         //Set the audiofile name
                
            do{
                let fileExists = try audioFilename.checkResourceIsReachable()           //Check if there is already a file with such a name
                
                if fileExists {
                    i+=1;                                                               //Increment i
                    name.removeAll();                                                   //Resets the name to Shrecoding
                    name="Shrecording"
                    }
            }
            catch {
                    found = true                                                        //Prints the recording name
                    if allowRecordingConsole {
                    print("selected name:", name)
                    }
                    }
                
            }
        
        let settings = [                                                                //Audioformat
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        
        do{                                                                             //Sets the audio recorder as output to audiofilename with previous settings
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()                                                      //Record audio
        }
        catch{
            stopRecording()                                                             //stopRecording if any error occurs - probably this prevents the pausing at incomming call
            finishRecording(success: false, name: "CRASHED", place: "CRASH", author: "CRASH")
        }
        
    }
    
    func pauseRecoreder()
    {
        audioRecorder.delegate = self
        audioRecorder.pause()
        state = State.paused
    }
    
    func resumeRecorder()
    {
        audioRecorder.delegate = self
        audioRecorder.record()
        state = State.recording
    }
    
    func getDocumentDirectory() -> URL {                                                 //Returns the documents directory
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    func stopRecording()
    {
        for e in flags
        {
            if e.end > audioRecorder.currentTime {                                         //Loops through the flags and sets the current time as end if the end time is larger
                e.end = audioRecorder.currentTime;
            }
        }
        dur = audioRecorder.currentTime
        audioRecorder.stop()
        audioRecorder = nil                                                                //Dereference, prevents dangling pointers, also in swift???
    }
    
    func finishRecording(success: Bool, name: String,place: String, author: String) {
        state = State.nothing
        let recording = Recording()
        if name != ""
        {
        recording.name = name;
        }
        if allowRecordingConsole {
        print("New recording was named: ",recording.name)
        }
        let date = NSDate();
        let calendar = NSCalendar.current;
        recording.date = String(calendar.component(.day, from: date as Date))  + String(". ") + String(calendar.component(.month, from: date as Date)) + String(". ") + String(calendar.component(.year, from: date as Date)) + String(" | ") + String(calendar.component(.hour, from: date as Date)) + String(":") + String(calendar.component(.minute, from: date as Date));

        if name == ""
        {
            recording.name = String(calendar.component(.day, from: date as Date))  + String(". ") + String(calendar.component(.month, from: date as Date)) + String(". ") + String(calendar.component(.year, from: date as Date));
        }
        
        recording.duration = dur
        
        
        if place == ""
        {
            recording.place = defaultPlace
        }
        else
        {
            recording.place = place
        }
        
        if author == ""
        {
            recording.author = defaultAuthor
        }
        else
        {
            recording.author = author
        }
        recording.filename = recName;
        recording.markers = flagAmount;
        
        recording.markedTimes = flags;
        
        recordings.append(recording)
        availableRecordings += 1
        save()
        flagAmount=0;
        flags.removeAll();
    }
    
    func getSound(filename: String) -> AVAudioPlayer
    {
        do{
        let sound = try AVAudioPlayer(contentsOf: getDocumentDirectory().appendingPathComponent(filename))
        return sound
        }
        catch
        {
        return AVAudioPlayer()
        }
        
    }

    
    func play(filename: String)
    {
        do{

            if allowPlaybackConsole {
            print("Playing sound at: ", filename)
            }
            let sound = try AVAudioPlayer(contentsOf: getDocumentDirectory().appendingPathComponent(filename))
            sound.delegate = self
            recordedSound = sound
            sound.play()
            state = State.playing
        }
        catch
        {
            print("Failed to play file.")
        }
    }
    
    func playPart(filename: String ,start: Double, end: Double)
    {
  
        print("Playing item.")
            let item =  AVPlayerItem(url: getDocumentDirectory().appendingPathComponent(filename))
                
            partSound =  AVPlayer(playerItem: item)
            partSound?.play()
            state = State.playing;
            let targetTime = CMTimeMakeWithSeconds(start, Int32(NSEC_PER_SEC))
            item.seek(to: targetTime )
        var timeArray : [NSValue] = []
        timeArray.append(CMTimeMakeWithSeconds(end, Int32(NSEC_PER_SEC)) as NSValue)
        
        partSound?.addBoundaryTimeObserver(forTimes: timeArray , queue: DispatchQueue.main) {
            if self.allowPlaybackConsole{
            print("End of block played!")
            }
            self.stop()
        }

    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("Playing over.")
        state = State.nothing
    }

    
    func stop()
    {
        
        if partSound != nil {
            partSound?.pause();
            partSound = nil
            state = State.nothing
        }
        
        if recordedSound != nil {
            recordedSound.stop()
            recordedSound = nil
            state = State.nothing
        }
    }
    
    func deleteRecording(filename: String)
    {
        let path = getDocumentDirectory().appendingPathComponent(filename)
        do
        {
        try FileManager.default.removeItem(at: path)
        }
        catch
        {
            print("error removing file");
        }
    }
    
    func save()
    {
        let file = "save.txt"
        
        var text = String(availableRecordings)
        text.append("\n")
        text.append(defaultAuthor)
        text.append("\n")
        text.append(defaultPlace)
        text.append("\n")
        text.append(String(flagDuration))
        text.append("\n")

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let path = dir.appendingPathComponent(file)
            
            do
            {
             try FileManager.default.removeItem(at: path)
            }
            catch
            {
                
            }
            for element in recordings{
                text.append(element.name);
                text.append("\n");
                text.append(element.filename);
                text.append("\n");
                text.append(element.date);
                text.append("\n");
                text.append(element.place);
                text.append("\n");
                text.append(element.author);
                text.append("\n");
                text.append(element.shouldBeDeleted);
                text.append("\n");
                text.append(String(element.duration));
                text.append("\n");
                text.append(String(element.markers));
                text.append("\n");
                if element.markers > 0
                {
                for e in element.markedTimes
                {
                    text.append(String(e.defaultTime))
                    text.append(" ");
                    text.append(String(e.start))
                    text.append(" ");
                    text.append(String(e.end))
                    text.append(" ");
                    text.append(e.filename)
                    text.append("~");
                    
                }
                }
                else
                {
                    text.append(String(0.000));
                    text.append(" ");
                    text.append(String(0.000));
                    text.append(" ");
                    text.append(String(0.000));
                    text.append(" ");
                    text.append("Unsaved");
                    text.append("~");
                    
                }
                text.append("\n");
            }
            
            
            do{
                try text.write(to: path, atomically: true, encoding: String.Encoding.utf8)
                if allowSaveAndLoadConsole {
                print("Saving:")
                print(text);
                print("********")
                }
            }
            catch{
                
            }
        }
    }
    
    func load()
    {
        let file = "save.txt"
        
        var text : String

        recordings.removeAll();
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let path = dir.appendingPathComponent(file)
            
            
            do{
                text = try String(contentsOf: path, encoding: String.Encoding.utf8)
                if allowSaveAndLoadConsole {
                print("Loading:")
                print(text)
                print("********")
                }
                let stringArray = text.components(separatedBy: "\n")
                availableRecordings = Int(stringArray[0])!
                defaultAuthor = stringArray[1]
                defaultPlace = stringArray[2]
                flagDuration = Int(stringArray[3])!
                var i: Int = 0;
                var name: String="";
                var filename: String="";
                var date: String="";
                var place: String="";
                var author: String="";
                var times: [Flag]=[];
                var marks: Int = 0;
                var duration: Double = 0;
                var shouldbe: String = "Yes";
                for string in stringArray
                {
                    if string != ""
                    {
                        if i>3
                        {
                            switch (i-3) % 9 {
                            case 1:
                                name = string; break;
                            case 2:
                                filename = string; break;
                            case 3:
                                date = string; break;
                            case 4:
                                place = string; break;
                            case 5:
                                author = string; break;
                            case 6:
                                shouldbe = string; break;
                            case 7:
                                duration = Double(string)!; break;
                            case 8:
                                marks = Int(string)!;
                                break;
                            case 0:
                                let flagElements = string.components(separatedBy: "~");
                                for element in flagElements
                                {
                                    if element != ""
                                    {
                                        let stuff = element.components(separatedBy: " ")
                                        let flag = Flag()
                                        
                                        flag.defaultTime = Double(stuff[0])!
                                        flag.start = Double(stuff[1])!
                                        flag.end = Double(stuff[2])!
                                        flag.filename = stuff[3]

                                        times.append(flag);
                                        
                                        
                                    }
                                }
                                let recording = Recording()
                                recording.name = name;
                                recording.filename = filename;
                                recording.date = date;
                                recording.place = place;
                                recording.author = author;
                                recording.duration = duration;
                                recording.markers = marks;
                                recording.markedTimes = times;
                                recording.shouldBeDeleted = shouldbe;
                                recordings.append(recording)
                                times.removeAll();
                                break;
                            default:
                                print("WTF"); break;
                            }
                        }
                    }
                    i+=1;
                }
            }
            catch{
                print("Error")
            }
        }
    }
    
    func saveItem(filename: String, pre: String, start: Double, end: Double) -> String
    {
        
        let asset = AVAsset.init(url: getDocumentDirectory().appendingPathComponent(filename))
        
            
        var prefix: String = "";
        prefix = pre.replacingOccurrences(of: " ", with: "_");
        prefix = prefix.replacingOccurrences(of: ".", with: "");
        
        
        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputFileType = AVFileTypeAppleM4A
        
        var trimmed = getDocumentDirectory().appendingPathComponent("NOPE");
        
        var name: String = prefix;
        var i: Int = 0;
        var found: Bool = false;
        while(!found)
        {
            name.append(String(i))
            name.append(".m4a");
            
            recName = name;
            
            trimmed = getDocumentDirectory().appendingPathComponent(name)
            
            do{
                let fileExists = try trimmed.checkResourceIsReachable()
                
                if fileExists {
                    i+=1;
                    name.removeAll();
                    name=prefix
                }
            }
            catch {
                found = true
                    print("selected name:", name)
                }
        }
        
        exporter.outputURL = trimmed
        
        let StartTime = CMTimeMake(Int64(start), 1)
        let EndTime = CMTimeMake(Int64(end), 1)
        let exportTimeRange = CMTimeRangeFromTimeToTime(StartTime, EndTime)
        
        exporter.timeRange = exportTimeRange
        
        exporter.exportAsynchronously(completionHandler: {
            switch exporter.status {
            case AVAssetExportSessionStatus.failed: print("Error exporting file: ", exporter.error ?? "PLUF")
            break;
            case AVAssetExportSessionStatus.cancelled: print("Export Cancelled")
            break;
            default: print("Export completed!")
                
            }
            
        })
        
        return name;
        
    }
    
    func listRecordings()
    {
        if allowListRecordings{
        print("list:");
        for element in recordings
        {
            element.printInfo();
    
        }
        print("end of list")
        }
    }
    
}
