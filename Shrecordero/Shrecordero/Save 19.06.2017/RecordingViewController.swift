//
//  ViewController.swift
//  Shrecordero
//
//  Created by Vlastimil Blazek, FORTES Enterprise on 18.01.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit
import AVFoundation

class RecordingViewController: UIViewController {
    
    var recording: Recording! = nil
    var track: Int = 0
    var recorder: Recorder! = nil
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var DurationLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var PlaceLabel: UILabel!
    @IBOutlet weak var FlagsLabel: UILabel!
    @IBOutlet weak var AuthorLabel: UILabel!
    
    @IBOutlet weak var Stepper: UIStepper!
    @IBOutlet weak var FlagLabel: UILabel!
    
    @IBOutlet weak var Slider: UISlider!
    @IBOutlet weak var StopButton: UIButton!
    @IBOutlet weak var PlayButton: UIButton!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var Switch: UISwitch!
    
    
    
    @IBOutlet weak var ShareButton: UIButton!
    var currentMarker = -1;

    @IBAction func Moved(_ sender: UISwitch) {

        if sender.isOn {
        recording.shouldBeDeleted = "No"
        }
        else
        {
        recording.shouldBeDeleted = "Yes"
        }
        
        print("switch");
        
        recorder.save()
    }
    
    func getDocumentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    @IBAction func SharePressed(_ sender: Any) {
        let firstActivityItem = "I have just made new fantastic recording with Shrecordero!"
        
        
        var sharedObject = getDocumentDirectory().appendingPathComponent(recording.filename);
        
        if currentMarker > -1 {
            sharedObject = getDocumentDirectory().appendingPathComponent(recording.markedTimes[currentMarker].filename);
        }
        
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, sharedObject], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        

        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func DeletePressed(_ sender: Any) {
        if currentMarker > -1{
        let alert = UIAlertController(title: "Delete Marker", message: "Are you sure you want to delete this marker? ", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
            if self.recording.markedTimes[self.currentMarker].filename != "Unsaved"
            {
                self.recorder.deleteRecording(filename: self.recording.markedTimes[self.currentMarker].filename)
            }
            self.recording.markedTimes.remove(at: self.currentMarker)
            self.recording.markers -= 1
            self.recorder.save()
            self.currentMarker = -1
            self.Stepper.maximumValue -= 1
            self.Stepper.value = 0
            
            let step = UIStepper();
            step.value = 0;
            self.StepperChange(step)
            self.FlagsLabel.text =  String(self.recording.markers);
            }
        ))
        
        present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Delete Recording", message: "Are you sure you want to delete this whole recording? This will also delete all unsaved markers!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
                
                var toErase: [Int] = []
                
                self.recorder.deleteRecording(filename: self.recording.filename)
                self.recording.filename = "Deleted"
                
                
            
                for index in 0...self.recording.markedTimes.count-1 {
                    if self.recording.markedTimes[index].filename == "Unsaved"
                    {
                        toErase.append(index)
                    }
                }
                
                var mod: Int = 0
                
                for index in toErase
                {
                    self.recording.markedTimes.remove(at: index-mod)
                    self.Stepper.maximumValue -= 1
                    self.recording.markers -= 1
                    mod += 1
                }
                
                self.recorder.save();
                self.FlagsLabel.text = String(self.recording.markers);
                
                self.SaveButton.setTitle("FULL RECORDING DELETED", for: .normal);
            }
            ))
            
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    func addTextField(textField: UITextField!){
        textField.placeholder = "Name of recording"
    }
    
    @IBAction func SavePressed(_ sender: Any) {
        
        let alert = UIAlertController(title: "Save Marker", message: "Select name of your marker: ", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: addTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
          
            self.recording.markedTimes[self.currentMarker].filename = self.recorder.saveItem(filename: self.recording.filename, pre: self.recording.name + alert.textFields![0].text!, start: self.recording.markedTimes[self.currentMarker].start, end: self.recording.markedTimes[self.currentMarker].end)
            self.recorder.save();
            self.SaveButton.isEnabled = false;
            var toDisplay = self.recording.markedTimes[self.currentMarker].filename
            toDisplay = toDisplay.replacingOccurrences(of: self.recording.name, with: "")
            toDisplay = toDisplay.replacingOccurrences(of: "0", with: "")
            
            self.SaveButton.setTitle(toDisplay, for: .normal)
            self.SaveButton.setTitleColor(UIColor(red: 0/255, green: 49/255, blue: 88/255, alpha: 1.0), for: .normal)
            
            self.ShareButton.isHidden = false;
 
        }
        ))
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func StepperChange(_ sender: UIStepper) {
        if sender.value > 0
        {
        var sentence = String(Double(round(10*recording.markedTimes[Int(sender.value)-1].start)/10))
        sentence.append(" TO ")
        sentence.append(String(Double(round(10*recording.markedTimes[Int(sender.value)-1].end)/10)))
        FlagLabel.text = sentence
        Slider.value = Float(recording.markedTimes[Int(sender.value)-1].defaultTime/recording.duration);
        currentMarker = Int(sender.value)-1;
            if recording.markedTimes[Int(sender.value)-1].filename == "Unsaved"
            {
                SaveButton.isEnabled = true;
                SaveButton.setTitle("SAVE THIS MARKER", for: .normal)
                SaveButton.setTitleColor(UIColor(red: 171/255, green: 21/255, blue: 46/255, alpha: 1.0), for: .normal)
                ShareButton.isHidden = true;
            }
            else
            {
                SaveButton.isEnabled = false;
                var toDisplay = recording.markedTimes[currentMarker].filename
                toDisplay = toDisplay.replacingOccurrences(of: recording.name, with: "")
                toDisplay = toDisplay.replacingOccurrences(of: "0", with: "")
                
                SaveButton.setTitle(toDisplay, for: .normal)
                SaveButton.setTitleColor(UIColor(red: 0/255, green: 49/255, blue: 88/255, alpha: 1.0), for: .normal)
                ShareButton.isHidden = false;
            }

        }
        else
        {
            FlagLabel.text = "PLAY FULL RECORDING"
            Slider.value = 0.0;
            currentMarker = -1;
            if recording.filename != "Deleted"
            {
            SaveButton.setTitle("FULL RECORDING AVAILABLE", for: .normal);
            ShareButton.isHidden = false;
            SaveButton.setTitleColor(UIColor(red: 0/255, green: 49/255, blue: 88/255, alpha: 1.0), for: .normal)
            }
            else
            {
            SaveButton.setTitle("FULL RECORDING DELETED", for: .normal);
            ShareButton.isHidden = true;
            SaveButton.setTitleColor(UIColor(red: 171/255, green: 21/255, blue: 46/255, alpha: 1.0), for: .normal)
            }
            SaveButton.isEnabled = false;
            
            }
    }
    
    @IBAction func PlayPressed(_ sender: Any) {
        PlayButton.alpha = 0.2;
        StopButton.alpha = 1;
        if currentMarker > -1
        {
            if recording.markedTimes[currentMarker].filename == "Unsaved"
            {
                recorder.playPart(filename: recording.filename, start: recording.markedTimes[currentMarker].start, end: recording.markedTimes[currentMarker].end)
            }
            else
            {
                recorder.play(filename: recording.markedTimes[currentMarker].filename)
            }
        }
        else
        {
            recorder.play(filename: recording.filename)
        }
        playAudio();
    }
    
    @IBAction func StopPressed(_ sender: Any) {
    
        recorder.stop()
    }
    
    var updater : CADisplayLink! = nil
    
    func playAudio() {
        
        updater = CADisplayLink(target: self, selector: #selector(RecordingViewController.trackAudio))
        updater.preferredFramesPerSecond = 1
        updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        
    }
    
    func trackAudio() {
        if recorder.state == Recorder.State.nothing {
            PlayButton.alpha = 1;
            StopButton.alpha = 0.2;
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StopButton.alpha = 0.2;
        
        recording = recorder.recordings[track-1];
        NameLabel.text=recording.name;
        
        let duration = Double(round(10*recording.duration)/10)
        DurationLabel.text = String(duration) + " seconds"

        DateLabel.text = recording.date;
        PlaceLabel.text = recording.place;
        FlagsLabel.text = String(recording.markers);
        AuthorLabel.text = recording.author;
        recorder.listRecordings()
        Stepper.value=0;
        Stepper.maximumValue=Double(recording.markers);
        Stepper.minimumValue=0;

        FlagLabel.text = "PLAY FULL RECORDING"

        if recording.filename != "Deleted"
        {
            SaveButton.setTitle("FULL RECORDING AVAILABLE", for: .normal);
            ShareButton.isHidden = false;
        }
        else
        {
            SaveButton.setTitle("FULL RECORDING DELETED", for: .normal);
            ShareButton.isHidden = true;
        }
        SaveButton.isEnabled = false;
        
        if recording.shouldBeDeleted == "Yes"
        {
            Switch.isOn = false;
        }
        else
        {
            Switch.isOn = true;
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Erase"{
            print("ERASING");
            recorder.deleteRecording(filename: recording.filename)
            for r in recording.markedTimes
            {
                if r.filename != "Unsaved"
                {
                    recorder.deleteRecording(filename: r.filename)
                }
            }
            recorder.recordings.remove(at: track-1)
            recorder.availableRecordings-=1;
            recorder.save();
        }
        let svc = segue.destination as! ViewController
        svc.recorder = recorder;
        
    }
    
}


