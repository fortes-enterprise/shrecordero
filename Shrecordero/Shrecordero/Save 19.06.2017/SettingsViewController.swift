//
//  SettingsViewController.swift
//  Shrecordero
//
//  Created by FORTES Enterprise on 20.02.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit
import AVFoundation

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var PlaceField: UITextField!
    
    @IBOutlet weak var AuthorField: UITextField!
    
    @IBOutlet weak var FlagLabel: UILabel!
    
    @IBOutlet weak var FlagSlider: UISlider!
    
    var recorder: Recorder! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FlagSlider.value = Float(recorder.flagDuration)
        FlagLabel.text = "±" + String(Int(FlagSlider.value)) + " minutes"
        PlaceField.text = recorder.defaultPlace
        AuthorField.text = recorder.defaultAuthor

    }
    
    @IBAction func authorEdit(_ sender: Any) {
        if AuthorField.text == "Vlastimil Blažek" {
            performSegue(withIdentifier: "egg", sender: nil);
        }
    }
    
    @IBAction func FlagChanged(_ sender: UISlider) {
        FlagLabel.text = "±" + String(Int(round(sender.value))) + " minutes"
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier != "egg"
        {
        let svc = segue.destination as! ViewController
        svc.recorder.defaultAuthor=AuthorField.text!;
        svc.recorder.defaultPlace=PlaceField.text!;
        svc.recorder.flagDuration=Int(FlagSlider.value);
        svc.recorder.save();
        }
    }
    
    
}
