//
//  ViewController.swift
//  Shrecordero
//
//  Created by Vlastimil Blazek, FORTES Enterprise on 18.01.17.
//  Copyright © 2017 FORTES Enterprise. All rights reserved.
//

import UIKit
import CallKit
import AVFoundation
import MediaPlayer
//import CoreLocation       //ENABLE FOR GPS

class ViewController: UIViewController, CXCallObserverDelegate {                //ADD: CLLocationManagerDelegate    TO ENABLE GPS
    //private var locationManager = CLLocationManager()         //ENABLE FOR GPS
    
    //Control
    enum Input {
        case RecordPressed
        case StopPressed
        case Actualize
        case IncommingCall
        case CallEnded
    }
    
    //Properities
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var Stop: UIButton!
    @IBOutlet weak var Record: UIButton!
    @IBOutlet weak var SpaceLabel: UILabel!
    @IBOutlet weak var PrevButton: UIButton!
    
    @IBOutlet weak var NextButton: UIButton!
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet weak var PageLabel: UILabel!
    
    var track: Int = 1
    var page: Int = 1
    
    @IBOutlet weak var SelectButton1: UIButton!
    @IBOutlet weak var SelectButton2: UIButton!
    @IBOutlet weak var SelectButton3: UIButton!
    
    var recorder = Recorder()
    var callObserver = CXCallObserver()
    
    //Actions
    @IBAction func flagPressed(_ sender: Any) {
        recorder.addFlag()
    }
    
    @IBAction func recordPressed(_ sender: Any) {
        processInput(input: Input.RecordPressed)
    }

    
    @IBAction func stopPressed(_ sender: Any) {
        processInput(input: Input.StopPressed)
    }
    
    @IBAction func ErasePressed(_ sender: Any) {
        
        for element in recorder.recordings
        {
             recorder.deleteRecording(filename: element.filename)
             for e in element.markedTimes
             {
                if e.filename != "Unsaved"
                {
                    recorder.deleteRecording(filename: e.filename)
                }
             }
        }
        recorder.availableRecordings=0
        recorder.recordings.removeAll()
        recorder.save()
        processInput(input: Input.Actualize)
    }
    
    @IBAction func NextPage(_ sender: Any) {
        if page*3<recorder.availableRecordings
        {
        page += 1
        PageLabel.text = String(page) + " / " + String((abs(recorder.availableRecordings-1))/3+1)
        track=0
        processInput(input: Input.Actualize)
        }
    }
    
    
    @IBAction func PreviousPage(_ sender: Any) {
        if page != 1 {
            page -= 1
            PageLabel.text = String(page) + " / " + String((abs(recorder.availableRecordings-1))/3+1)
            track=0
            processInput(input: Input.Actualize)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
            callObserver.setDelegate(self, queue:nil)
            return true
    }
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall)
    {
        if call.hasEnded
        {processInput(input: Input.CallEnded)}
        else
        {processInput(input: Input.IncommingCall)}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /* //ENABLE FOR GPS
         if locationServicesEnabled {
         self.locationManager.delegate = self
         self.locationManager.requestAlwaysAuthorization()          //PROBABLY FIND SOMETHING ELSE AS ALLWAYS REQUEST
         self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
         self.locationManager.startUpdatingLocation()               //requestLocation() is maybe more suitable, "Requests the one-time delivery of the current location"
         self.distanceFilter = 100                                  //UPDATE ONLY IF MOVED BY 100m OR MORE
         }
         */
        recorder.listRecordings()
        recorder.recordingSession = AVAudioSession.sharedInstance()
        
        do{
            try recorder.recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recorder.recordingSession.setActive(true)
            recorder.recordingSession.requestRecordPermission() {[unowned self] allowed in DispatchQueue.main.async {
                if allowed {
                    self.recorder.permission=true
                }
                else{
                    self.recorder.permission=false
                }
                }
                
            }
        }
        catch{
            recorder.permission=false;
        }
        
        PageLabel.text = String(page) + " / " + String((abs(recorder.availableRecordings-1))/3+1)
        
        processInput(input: Input.Actualize)
        checkDiskSpace()
    }
    
    func listenVolumeButton() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
        } catch {
            print("some error")
        }

        print("adding new observer")
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func stopObserving()
    {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
        } catch {
            print("some error")
        }
        
        print("removing observer")
        audioSession.removeObserver(self, forKeyPath: "outputVolume")
    }
    
    var starting: Bool = false;
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "outputVolume" {
            
            let volumeView = MPVolumeView()
            let volume = AVAudioSession.sharedInstance().outputVolume
            if let view = volumeView.subviews.first as? UISlider {
               if volume < 0.05 || volume > 0.95 {
                    view.value=0.5;
                }
                else
                {
                    if !starting
                    {
                    print("New flag added");
                    recorder.addFlag();
                    }
                    starting=false;
                }
            }

        }
    }
    

    func showButtons()
    {
        print("Available Recording: ", recorder.availableRecordings)
        SelectButton1.isHidden = true
        SelectButton2.isHidden = true
        SelectButton3.isHidden = true
        if recorder.availableRecordings>(page-1)*3
        {
            SelectButton1.isHidden = false
            if recorder.availableRecordings>(page-1)*3+1
            {
                SelectButton2.isHidden = false
                if recorder.availableRecordings>(page-1)*3+2
                {
                    SelectButton3.isHidden = false
                }
            }
        }
    }
    
	

    func addTextField(textField: UITextField!){
        textField.placeholder = "Name of recording"
    }
    
    func addTextFieldL(textField: UITextField!){
        textField.placeholder = recorder.defaultPlace
    }
    
    func addTextFieldA(textField: UITextField!){
        textField.placeholder = recorder.defaultAuthor
    }
    
    
    func processInput(input: Input)
    {
        switch input {
        case Input.RecordPressed:
            if recorder.state == Recorder.State.nothing
            {
                let volumeView = MPVolumeView()
                let volume = AVAudioSession.sharedInstance().outputVolume
                if let view = volumeView.subviews.first as? UISlider {
                    if volume < 0.05 || volume > 0.95 {
                        view.value=0.5;
                        starting=true;
                    }
                }
            recorder.startRecording()
            listenVolumeButton()
            }
            break
        case Input.StopPressed:
            switch recorder.state {
                case .playing:
                recorder.stop()
                break;
                case .recording:
                    
                stopObserving()
                
                let alert = UIAlertController(title: "Enter save info", message: "Enter Name, location and author:", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Discard", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.recorder.state=Recorder.State.nothing
                    self.processInput(input: Input.Actualize)
                }))
                alert.addTextField(configurationHandler: addTextField)
                alert.addTextField(configurationHandler: addTextFieldL)
                alert.addTextField(configurationHandler: addTextFieldA)
                
                var place: String = "";
                var name: String = "";
                var author: String = "";
                
                alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                    var textField = alert.textFields![0] as UITextField
                    name = textField.text!
                    textField = alert.textFields![1] as UITextField
                    place = textField.text!
                    textField = alert.textFields![2] as UITextField
                    author = textField.text!
                    self.recorder.finishRecording(success: true, name: name, place: place, author: author)
                    self.processInput(input: Input.Actualize)
                    self.page = (abs(self.recorder.availableRecordings-1))/3+1;
                    switch self.recorder.availableRecordings % 3 {
                    case 0: self.performSegue(withIdentifier: "ThirdButtonSegue", sender: nil); break;
                    case 1: self.performSegue(withIdentifier: "FirstButtonSegue", sender: nil); break;
                    case 2: self.performSegue(withIdentifier: "SecondButtonSegue", sender: nil); break;
                    default: print("WTF"); break;
                    }
                }))
                present(alert, animated: true, completion: nil)
                recorder.stopRecording()
                break
                default:
                print("Meaningless Input: Stop Button while nothing happens...")
                break;
            }
            break
            
        case Input.IncommingCall:
            switch recorder.state {
            case .playing:
                recorder.stop()
                break
            case .recording:
                recorder.pauseRecoreder()
                break
            case .paused:
                break
            default: break
                }
            break
            
        case Input.CallEnded:
            switch recorder.state {
            case .paused:
                recorder.resumeRecorder()
                break
            default: break
            }
            break
            
        default:
            PageLabel.text = String(page) + " / " + String((abs(recorder.availableRecordings-1))/3+1)
            if page == 1 {
                PrevButton.alpha = 0.2;
            }
            else
            {
                PrevButton.alpha = 1;
            }
            
            if page == abs(recorder.availableRecordings-1)/3 + 1{
                NextButton.alpha = 0.2;
            }
            else
            {
                NextButton.alpha = 1;
            }
            
            break
            
        }
        
        switch recorder.state {
        case .nothing:
            text.text="NON RECORDING"
            Stop.alpha = 0.2;
            Record.alpha = 1;
            break;
        case .recording:
            text.text="RECORDING"
            Stop.alpha = 1;
            Record.alpha = 0.2;
            break;
        case .paused:
            text.text="PAUSED"
            Stop.alpha = 0.2;
            Record.alpha = 1.0;
            break;
        default: break
        }
        
        var index: Int = 1
        for recording in recorder.recordings
        {
            if index > (page-1) * 3 && index < page*3 + 1{
                switch index - (page-1) * 3 {
                case 1:
                    SelectButton1.setTitle(recording.name, for: .normal)
                    if recording.shouldBeDeleted == "Yes" {
                        SelectButton1.setTitle(recording.name + "(!)", for: .normal)
                    }
                    break;
                case 2:
                    SelectButton2.setTitle(recording.name, for: .normal)
                    if recording.shouldBeDeleted == "Yes" {
                        SelectButton2.setTitle(recording.name + "(!)", for: .normal)
                    }
                    break;
                case 3:
                    SelectButton3.setTitle(recording.name, for: .normal)
                    if recording.shouldBeDeleted == "Yes" {
                        SelectButton3.setTitle(recording.name + "(!)", for: .normal)
                    }
                    break;
                default:
                    break;
                }
            }
            index += 1
        }
        
        showButtons()
    }
    
    func deviceRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
            else {
                // something failed
                return nil
        }
        return freeSize.int64Value
    }
    
    var updater : CADisplayLink! = nil
    
    func checkDiskSpace() {
        
        updater = CADisplayLink(target: self, selector: #selector(ViewController.actualizeDiskSpace))
        updater.preferredFramesPerSecond = 1
        updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        
    }
    
    func actualizeDiskSpace() {
        SpaceLabel.text = "AVAILABLE " + String(((deviceRemainingFreeSpaceInBytes())!/(500000*60))) + " H"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FirstButtonSegue" || segue.identifier == "SecondButtonSegue" || segue.identifier == "ThirdButtonSegue"
        {
        let svc = segue.destination as! RecordingViewController
        if segue.identifier == "FirstButtonSegue"{
            svc.track = (page-1)*3+1
        }
        if segue.identifier == "SecondButtonSegue"{
            svc.track = (page-1)*3+2
        }
        if segue.identifier == "ThirdButtonSegue"{
            svc.track = (page-1)*3+3
        }
        svc.recorder = recorder
        }
        
        if segue.identifier == "SettingsSegue" {
            let svc = segue.destination as! SettingsViewController
            svc.recorder = recorder
        }
    }
    
    /*  //ENABLE FOR GPS, HERE IM ONLY PRINTING THE COORDS
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
     if locationServicesEnabled {
        println("didFailWithError: \(error.description)")
        let errorAlert = UIAlertView(title: "Error", message: "Failed to Get Your Location", delegate: nil, cancelButtonTitle: "Ok")
        errorAlert.show()
     }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let newLocation = locations.last as CLLocation
     if locationServicesEnabled {
        println("current position: \(newLocation.coordinate.longitude) , \(newLocation.coordinate.latitude)")   //SHOULD BE WRITTEN TO PLACE INFO OF RECORDING
     }
    }
     
     //TO ENABLE GPS YOU ALSO HAVE TO ADD THE FOLLOWING TO INFO.PLIST
     <key>NSLocationAlwaysUsageDescription</key>
     <string>This application needs access to your location information</string>
     <key>NSLocationWhenInUseUsageDescription</key>
     <string>This application needs access to your location information</string>
     
    */
}


